#Copyright Johan S. R. Nielsen
#This file is part of the codinglib for Sage.
#This library is free software released under the GPL v3 or any later version as
#published by the Free Software Foundation.

r"""Functions for importing Codinglib remotely, loaded on-the-fly
directly from the repository. This is useful for loading Codinglib on
a public Sage server."""

from sage.misc.remote_file import get_remote_file
from sage.repl.preparse import preparse_file_named         

# These modules do not support being loaded dynamically with these functions
# (for some unknown reason)
exception_modules = [ "counting" ]

def import_code(code,name,add_to_sys_modules=False):
    """
    Import dynamically generated code as a module. code is the
    object containing the code (a string, a file handle or an
    actual compiled code object, same types as accepted by an
    exec statement). The name is the name to give to the module,
    and the final argument says wheter to add it to sys.modules
    or not. If it is added, a subsequent import statement using
    name will return this module. If it is not added to sys.modules
    import will try to load it in the normal fashion.

    import foo

    is equivalent to

    foofile = open("/path/to/foo.py")
    foo = importCode(foofile,"foo",1)

    Returns a newly generated module.
    """
    import sys,imp

    module = imp.new_module(name)

    exec(code in module.__dict__)
    if add_to_sys_modules:
        sys.modules[name] = module

    return module

def retrieve_remote_module(baseurl, name):
    """Retrieves a remote Sage module with URL ''baseurl/name.sage'', preparses
    it to Python code and loads it to the system modules. Use 'from name import
    *' after calling ''retrieveRemoteModule''."""
    sageFile = get_remote_file(baseurl + name +".sage", verbose=False)

    #Preprocess it to a py file
    pyFile = preparse_file_named(sageFile)
    #Read and add line to load sage library
    lines = open(pyFile).readlines()
    lines.insert(3,"from sage.all_cmdline import *\n")
    return import_code("".join(lines), name, 0)

def retrieve_codinglib(baseurl):
    """Retrieve the entire codinglib using the all.py file in the remote
    directory."""
    import re, imp, sys
    # Create a new codinglib module in the system
    codinglib = imp.new_module("codinglib")
    sys.modules["codinglib"] = codinglib
    
    # Fetch baseurl/all.py and load all modules in it
    allFile = get_remote_file(baseurl + "/all.py", verbose=False)
    allLines = open(allFile).readlines()
    modules = []
    matcher = re.compile(r"from codinglib\.([^ ]*) import")
    module = None
    imports = []
    last=0
    for i in range(0, len(allLines)):
        matching = matcher.match(allLines[i])
        if matching:
            # Found new module, add the import statements for the last module
            if module:
                 imports.append( "".join(allLines[last:i]).replace("\\","")\
                                                          .replace("\n",""))
            # Retrieve and import the new into codinglib
            moduleName = matching.group(int(1))
            if not moduleName in exception_modules:
                module = retrieve_remote_module(baseurl, moduleName)
                codinglib.__dict__[moduleName] = module
                sys.modules["codinglib."+moduleName] = module
            last = i
    # Send all import statements from all.py back to the caller
    return imports
