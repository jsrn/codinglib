"""Generates the package overview in the readme"""
import pkgutil
import codinglib

def print_module_docstrings():
    ignore_modules = [ "all" ]

    summary = []

    for importer, modname, ispkg in pkgutil.iter_modules(codinglib.__path__):
        if not modname in ignore_modules :
            module = __import__("codinglib."+modname, fromlist="dummy")
            doc = module.__doc__
            if doc is None:
                doc = "NO DOC STRING"
            doclines = doc.split('\n')
            doc = "\n\t".join(doclines)
            summary.append("-  **"+ modname +"**: "+ doc)
            summary.append("")

    print("\n".join(summary))

if __name__ == "__main__":
    print_module_docstrings()
