"""Module for a simple thread pool, along with invoking workers over SSH.
"""
from threading import Thread
import subprocess
import time

class Manager:

    WAIT_TIME = 1

    def __init__(self, workerPool):
        self.workerPool = workerPool

    def perform_tasks(self, tasks):
        for task in tasks:
            pending = True
            while pending:
                worker = self.workerPool.get_free()
                if worker:
                    worker.perform(task)
                    pending = False
                else:
                    time.sleep(Manager.WAIT_TIME)
        while not self.workerPool.all_done():
            time.sleep(Manager.WAIT_TIME)


class WorkerPool:

    def __init__(self):
        self.workers = set()

    def add(self, worker):
        self.workers.add(worker)

    def count(self):
        return len(self.workers)

    def get_free(self):
        """Return a free worker. Returns None if no such worker exist in the pool"""
        for worker in self.workers:
            if worker.is_free():
                return worker
        return None

    def all_done(self):
        """Returns whether all current tasks are done"""
        for worker in self.workers:
            if not worker.is_free():
                return False
        return True
        


class Worker:
    """Represents a free machine to be set to work by giving it a tasks. A
    worker wraps threads in that, each time a worker receives a task, he will
    start a thread to perform the task."""

    def __init__(self):
        self._is_free = True
        self._active_thread = None

    def perform(self, task):
        """Let the worker run the given function. Return the output."""
        self._active_thread = Thread(target=task)
        self._active_thread.start()

    def is_free(self):
        return (self._active_thread is None) or (not self._active_thread.is_alive())

    def wait_for(self, timeout=None):
        """Halts the calling thread until the Worker has finished his current task.
        Similar to Thread.join()"""
        if self._is_free:
            return
        self._active_thread.join(timeout)
        
        
    
    
class Task:
    """A task for a worker. Runnable so it can be served directly as target for a Thread."""
    # Override this
    def __call__(self):
        raise NotImplementedError

class TaskFunction(Task):
    def __init__(self, f):
        self.f = f
    def __call__(self):
        self.f()
    def __str__(self):
        return "TaskFunction: %s" % self.f

class TaskShellCommand(Task):
    def __init__(self, cmd):
        self.cmd = cmd
    def __call__(self):
        returncode = subprocess.call(self.cmd, shell=True)
        self._returncode = returncode
    def __str__(self):
        return "TaskShellCommand: %s" % self.cmd



class WorkerSSH(Worker):
    """A worker calling a shell command over ssh.
    Note, this will be non-interactive so ssh-agent or similar must be
    running."""

    def __init__(self, host):
        Worker.__init__(self)
        self.host = host

    def perform(self, task):
        if not hasattr(task, 'cmd'):
            raise NotImplementedError("Only shell command tasks allowed for a WorkerSSH")
        ssh_task = TaskShellCommand("ssh "+ self.host + " \"" + task.cmd + "\"")
        Worker.perform(self, ssh_task)
